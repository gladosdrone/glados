#ifndef BME280_BME280_I2C_H
#define BME280_BME280_I2C_H

#include "bme280.h"
#include <string.h>
#include <stdlib.h>

extern I2C_HandleTypeDef hi2c2;

int8_t init_BME280(void);
double get_pressure(void);
double get_temperature(void);
double get_all(void);
double get_altitude(double *press, double *temp);

void user_delay_ms(uint32_t period);
int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

#endif //BME280_BME280_I2C_H
