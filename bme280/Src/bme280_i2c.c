#include "bme280_i2c.h"
#include <math.h>

struct bme280_dev dev;
int8_t result = BME280_OK;

int8_t init_BME280(void){
  int8_t  result = BME280_OK;
  uint8_t settings_sel;

  dev.dev_id = 0x77<<1; //I2C Address of BME280
  dev.intf = BME280_I2C_INTF; // I2C Interface
  dev.read = user_i2c_read; // Function for reading Data via I2C
  dev.write = user_i2c_write; // Function for writing Data via I2C
  dev.delay_ms = user_delay_ms; // Delay Function (ms)

  result = bme280_init(&dev); // initialize sensor
  if(result != BME280_OK)
  {
    return result;
  }

  //Set Oversampling and Filter configurations
  dev.settings.osr_h = BME280_NO_OVERSAMPLING;
  dev.settings.osr_p = BME280_OVERSAMPLING_16X;
  dev.settings.osr_t = BME280_OVERSAMPLING_2X;
  dev.settings.filter = BME280_FILTER_COEFF_16;
  settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;
  result = bme280_set_sensor_settings(settings_sel, &dev); //Transmit Settings to Sensor
  if(result != BME280_OK)
  {
    return result;
  }

  return result;
}

void user_delay_ms(uint32_t period){
  HAL_Delay(period);
}

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len){
  int8_t  result = 0; // Return 0 for Success, non-zero for failure

  int8_t *buf;
  buf = malloc(len+1);
  buf[0] = reg_addr;
  memcpy(buf+1, reg_data, len); // Copy Register Data into Buffer

  //Transmit Register Data in buffer to Sensor
  if(HAL_I2C_Master_Transmit(&hi2c2, dev_id, (uint8_t*)buf, len+1, HAL_MAX_DELAY) != HAL_OK) return 1;

  free(buf);

  return result;
}

int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len){
  int8_t  result = 0; // Return 0 for Success, non-zero for failure

  //Transmit Register Address to be read from
  if(HAL_I2C_Master_Transmit(&hi2c2, dev_id, &reg_addr, 1, HAL_MAX_DELAY) != HAL_OK) result = 1;
  //Read Register Data of Register Adress
  if(HAL_I2C_Master_Receive(&hi2c2, dev_id | 0x01, reg_data, len, HAL_MAX_DELAY) != HAL_OK) result = 1;

  return result;
}

double get_pressure(void){
  struct bme280_data comp_data;
  result = bme280_set_sensor_mode(BME280_FORCED_MODE, &dev);
  dev.delay_ms(20);
  result = bme280_get_sensor_data(BME280_PRESS, &comp_data, &dev);
  if(result != BME280_OK){
    return result;
  }
  return comp_data.pressure/100; //hPa
}

double get_temperature(void){
  struct bme280_data comp_data;
  result = bme280_set_sensor_mode(BME280_FORCED_MODE, &dev);
  dev.delay_ms(20);
  result = bme280_get_sensor_data(BME280_TEMP, &comp_data, &dev);
  if(result != BME280_OK){
    return result;
  }
  return comp_data.temperature; //°C
}

double get_altitude(double *press, double *temp){
  struct bme280_data comp_data;
  result = bme280_set_sensor_mode(BME280_FORCED_MODE, &dev); // Set Forced Mode
  dev.delay_ms(40); // Wait for measurement
  result = bme280_get_sensor_data(BME280_ALL, &comp_data, &dev); // Get Data
  *press = comp_data.pressure/100; //hPa
  *temp = comp_data.temperature; //°C
  if(result != BME280_OK){
    return result;
  }

  const float sea_press = 1013.25;

  //Calculate and return altitude
  return ((pow((sea_press/ *press ), 1/5.257) - 1.0) * (*temp + 273.15)) / 0.0065;
}