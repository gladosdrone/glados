/**
  ******************************************************************************
  * @file    MPU9150.h
  * @author  Julian Kandlhofer V3.0
  *          Manuel Lang V2.0
  * @version V3.0
  * @date    16.10.2019
  * @brief   This file provides functions for:
  *           + Initialization of the MPU9150 sensor
	*						+ reading measurement values of gyroscope, accelerometer,
  *						  temperature sensor and magnetometer
	*						+ Applying a complementary filter to combine the measurements
  *						  into rotation angles
  ******************************************************************************
  */

#ifndef __MPU9150_H
#define __MPU9150_H

#include "math.h"
#include "stm32f1xx_hal.h"

#define bytes_to_u16(MSB,LSB) ((uint16_t) ((uint8_t) MSB<<8u) | ((uint8_t) LSB))

// period of control loop [ms]
#define SYSTICK_TIME 5
// % of Gyro values being used
// 1 - Gyro_coeff = % of Accellerometer usage
#define COMP_GYRO_COEFF 0.95
#define RAD_TO_DEG 57.29577951 /* 180/pi*/

/* MPU-I2C-Addresses */
#define	MPU_I2C_ADDR 0xD2
// Address of Magnetometer on internal I2C-Bus
#define MPU_MAG_ADDR 0x18

/* MPU-Config-Registers */
#define MPU9150_CONFIG             0x1A
#define MPU9150_PWR_MGMT_1         0x6B
#define MPU9150_GYRO_CONFIG        0x1B
#define MPU9150_ACCEL_CONFIG       0x1C
#define MPU9150_MAG_CNTL           0x0A
#define MPU9150_SMPLRT_DIV      0x19
#define MPU9150_INT_PIN_CFG     0x37
#define MPU9150_INT_PIN_CFG     0x37
#define MPU9150_INT_ENABLE      0x38

/* MPU-ACCEL-MEASUREMENT-Registers */
#define MPU9150_ACCEL_XOUT_H       0x3B
#define MPU9150_ACCEL_XOUT_L       0x3C
#define MPU9150_ACCEL_YOUT_H       0x3D
#define MPU9150_ACCEL_YOUT_L       0x3E
#define MPU9150_ACCEL_ZOUT_H       0x3F
#define MPU9150_ACCEL_ZOUT_L       0x40

/* MPU-GYRO-MEASUREMENT-Registers */
#define MPU9150_GYRO_XOUT_H        0x43
#define MPU9150_GYRO_XOUT_L        0x44
#define MPU9150_GYRO_YOUT_H        0x45
#define MPU9150_GYRO_YOUT_L        0x46
#define MPU9150_GYRO_ZOUT_H        0x47
#define MPU9150_GYRO_ZOUT_L        0x48

/* MPU-TEMP-MEASUREMENT-Registers */
#define MPU9150_TEMP_OUT_H         0x41
#define MPU9150_TEMP_OUT_L         0x42

/* MPU-MAG-MEASUREMENT-Registers*/
#define MPU9150_MAG_STATUS1       0x02
#define MPU9150_MAG_STATUS2       0x09
#define MPU9150_MAG_XOUT_H        0x03
#define MPU9150_MAG_XOUT_L        0x04
#define MPU9150_MAG_YOUT_H        0x05
#define MPU9150_MAG_YOUT_L        0x06
#define MPU9150_MAG_ZOUT_H        0x07
#define MPU9150_MAG_ZOUT_L        0x08

// Registers for enableing magnetometer
#define MPU9150_USER_CTRL_REG     0x6A
#define MPU9150_INT_PIN_CFG_REG   0x37

#define BIT_SLV0_DELAY_EN               (0x01)
#define BIT_SLV1_DELAY_EN               (0x02)
#define BIT_I2C_BYPASS_EN               (0x02)
#define BIT_I2C_MST_EN                  (0x20)
#define BIT_PWR_MGMT1_SLEEP             (0x40)
#define BIT_WAIT_FOR_ES                 (0x40)
#define BIT_I2C_MST_VDDIO               (0x80)
#define BIT_SLAVE_RW                    (0x80)
#define BIT_SLAVE_EN                    (0x80)
#define BIT_DMP_EN                      (0x80)

#define MPU_BIT_I2C_MASTER_EN     0b10
#define MPU_BIT_I2C_BYPASS_EN     0b10

#define MPU9150_COMP_POWER_DOWN     (0x00)
#define MPU9150_COMP_SINGLE_MEASURE (0x01)
#define MPU9150_COMP_SELF_TEST      (0x08)
#define MPU9150_COMP_FUSE_ROM       (0x0F)
#define MPU9150_COMP_WHOAMI_ANSWER  (0x48)  /**< MPU9X50 WHO_AM_I answer register */

#define MPU9150_COMP_WHOAMI_REG              (0x00)
#define MPU9150_COMP_DATA_START_REG          (0x03)
#define MPU9150_COMP_CNTL_REG                (0x0A)
#define MPU9150_COMP_ASAX_REG                (0x10)


#define MPU9150_I2C_MST_REG             (0x24)

#define MPU9150_SLAVE0_ADDR_REG         (0x25)
#define MPU9150_SLAVE0_REG_REG          (0x26)
#define MPU9150_SLAVE0_CTRL_REG         (0x27)
#define MPU9150_SLAVE1_ADDR_REG         (0x28)
#define MPU9150_SLAVE1_REG_REG          (0x29)
#define MPU9150_SLAVE1_CTRL_REG         (0x2A)

#define COMPASS_WHOAMI_REG              (0x00)
#define COMPASS_ST1_REG                 (0x02)
#define COMPASS_DATA_START_REG          (0x03)
#define COMPASS_ST2_REG                 (0x09)
#define COMPASS_CNTL_REG                (0x0A)
#define COMPASS_ASTC_REG                (0x0C)
#define COMPASS_ASAX_REG                (0x10)
#define COMPASS_ASAY_REG                (0x11)
#define COMPASS_ASAZ_REG                (0x12)

#define MPU9150_SLAVE1_DATA_OUT_REG     (0x64)
#define MPU9150_I2C_DELAY_CTRL_REG      (0x67)
#define MPU9150_YG_OFFS_TC_REG          (0x01)
#define MPU9150_EXT_SENS_DATA_START_REG (0x49)
// Base Sensitivity
// Can be adjusted with "range" on init
// g = 9.81m/s2
// [LSB/mg]
#define ACC_SENSITIVITY 16384.0f
// [LSB/(�/s)]
#define GYRO_SENSITIVITY 131.0f

/* Digital-Lowpass-Filter-Configuration */
typedef enum
{
  DLPF_260_Hz = 0,
  DLPF_184_Hz = 1,
  DLPF_94_Hz = 2,
  DLPF_44_Hz = 3,
  DLPF_21_Hz = 4,
  DLPF_10_Hz = 5,
  DLPF_5_Hz = 6
} mpu_bandwidth_dlpf;

/* ACCEL-Measurement-Range */
// Register Map Page 28
typedef enum													/****************************************/
{																			/*	 AFS_SEL	 *	 Full Scale Range			*/
	MR_2_g = 0,													/*			0		 	 *	     +/- 2 g					*/
	MR_4_g = 1,											  	/*			1		   *	     +/- 4 g					*/
	MR_8_g = 2,													/*			2		   *	     +/- 8 g					*/
	MR_16_g = 3													/*			3		   *	     +/- 16 g					*/
} mpu_acc_measurement_range;			/****************************************/

/* GYRO-Measurement-Range */
// Register Map Page 30
typedef enum								 					/***************************************/
{											 			 					/*	   FS_SEL	  *	  Full Scale Range	 */
  MR_250 = 0,								 					/*			 0		  *	    +/- 250 �/s	   	 */
  MR_500 = 1, 												/*			 1		  *	    +/- 500 �/s		   */
  MR_1000 = 2, 												/*			 2		  *	    +/- 1000 �/s		 */
  MR_2000 = 3												  /*			 3		  *	    +/- 2000 �/s		 */
} mpu_gyro_measurement_range;			/***************************************/

typedef struct {
    float x,y,z;
}xyz_type;

typedef struct {
    xyz_type gyro, accel, mag, mag_sens;
    float acc_sens, gyro_sens;
    float temperature;
    float pitch, bank, yaw;
}mpu_type;

extern mpu_type mpu;

// TODO: Implement Magnetometer
// TODO: Use DMA or Interrupt based reading of I2C values

int mpu_init(I2C_HandleTypeDef *i2c, mpu_bandwidth_dlpf dlpf);
int mpu_i2c_bypass(I2C_HandleTypeDef *i2c, int enable);
int mpu_accel_config(I2C_HandleTypeDef *i2c, mpu_acc_measurement_range range);
int mpu_gyro_config(I2C_HandleTypeDef *i2c, mpu_gyro_measurement_range range);
int mpu_mag_config(I2C_HandleTypeDef *i2c);
int mpu_read_acc_xyz(I2C_HandleTypeDef *i2c);
int mpu_read_gyro_xyz(I2C_HandleTypeDef *i2c);
int mpu_read_mag_xyz(I2C_HandleTypeDef *i2c);
int mpu_read_all(I2C_HandleTypeDef *i2c);
int mpu_complementaryFilter(void);

#endif
