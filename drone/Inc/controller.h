//
// Created by julian on 11.10.2019.
//

#ifndef DRONE_CONTROLLER_H
#define DRONE_CONTROLLER_H

#include "receiver.h"
#include "MPU9150.h"
#include "engines.h"
#include "pid.h"

typedef struct  {
    pid_type pitch_pid;
    pid_type bank_pid;

    float pitch_set;
    float bank_set;
    float yaw_set;

    float thrust_out;
    float pitch_out;
    float bank_out;
    float yaw_out;

    float battery_voltage;
} controller_type;

extern controller_type controller;

// initialize controller
void controller_init(void);
// measure input, run pid, output engines
void controller_loop(void);

#endif //DRONE_CONTROLLER_H
