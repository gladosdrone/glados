//
// Julian Kandlhofer, 5BHEL 13.10.2019
//

#ifndef DRONE_ENGINES_H
#define DRONE_ENGINES_H

#include <stm32f1xx_hal.h>

#include <stm32f1xx_hal_tim.h>

#define ENGINE_IDLE_POWER 20.f

// Engine Powers in [%]
// THIS IS A UNION, don't add stuff
typedef union power_type{
    float dir[4];
    struct {
        float front_right; // X8 , PA6
        float back_right;  // X9 , PA7
        float back_left;   // X10, PB0
        float front_left;  // X11, PB1
    };
} power_type;

typedef enum {
    ENGINE_NORMAL,
    ENGINE_KILL
} engine_state_type;

typedef struct {
  // desired output from controller
  power_type power;

  // actual output after limiting
  power_type out;

  // current state of engines
  engine_state_type state;
} engine_type;

extern engine_type engine;

void engines_init(void);
void engines_output(void);
void engines_stop(void);

#endif //DRONE_ENGINES_H
