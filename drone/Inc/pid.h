// Julian Kandlhofer, 2019-2020
// PID-Library

#ifndef DRONE_PID_H
#define DRONE_PID_H

// Period of Control Loop
#define DELTA_T 0.005f

typedef struct {
    // Tuning Parameters
    float kp, ki, kd;
    // max for integral windup
    float max;

    // Last Error for Diferential
    float elast;
    // Error Sum for Integral
    float esum;

    // previous output
    float u;

    // values to avoid float division
    // should not be accessed from outside
    float p_term, i_term, d_term;
} pid_type;

// Initialize PID-Struct
void pid_init(pid_type * pid, float kp, float ki, float kd, float max);
// Update Private Variables when changing Parameters
void pid_updateTerms(pid_type * pid);
// Calculate new Output
float pid_calculate(pid_type * pid, float error);


#endif //DRONE_PID_H
