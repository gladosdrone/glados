// Turnigy X9 Receiver Library
// Julian Kandlhofer 12.10.2019
//

#ifndef DRONE_RECEIVER_H
#define DRONE_RECEIVER_H


#include "stm32f1xx_hal.h"

// Timings for original TI PWMtoPPM board
// Input Capture Timings
// ftick = 4MHz
// PPM Period = 22.42ms
// tsignal + tsync = PPM Period = const
//#define TTOLERANCE 20                  // 5us
//#define TGAP 1200 + TTOLERANCE         // 300us
//#define TPULSE_MIN 3000 + TTOLERANCE   // 750us
//#define TPULSE_MAX 6000 + TTOLERANCE   // 1.5ms
//#define TSYNCMAX 50680 + TTOLERANCE    // 12.67ms
//#define TNEXT TPULSE_MAX+TGAP
//#define TMEAS_MAX TSYNCMAX+TGAP+TGAP+TGAP

#define TGAP 2000          // 400us
#define TPULSE_MIN 2680    // 670us
#define TPULSE_MAX 6000    // 1.5ms
#define TSYNCMAX 44100     // 11.04ms

#define TDELAY 420        // 105us: all measurements are 100us too low for some reason
#define TNEXT TPULSE_MAX+TGAP
#define TMEAS_MAX TSYNCMAX

// Max Pitch/Bank angle [°]
#define MAXANGLE 25

typedef struct {
  // measured pulse widths
  int pulse_width [9];

	union {
		// channel data
		float channel [9];
		// alias for the first four values
		struct{
			float bank;
			float pitch;
			float power;
			float yaw;
		};
	} input;

  int invalid;

  // Kills engines
  // left switch
  int killSwitch;

  // Resets PID controller
  // right switch
  int resetSwitch;

} receiver_type;

extern receiver_type receiver;

// initialize receiver struct
void receiver_init(void);
// run on timer interrupt to measure pulse width
void receiver_getChannels(void);
//  send channel values to SvVis
void receiver_sendChannelStatus(UART_HandleTypeDef *huart);
// convert pulse_width to channel values
void receiver_calculateInput(void);

#endif //DRONE_RECEIVER_H
