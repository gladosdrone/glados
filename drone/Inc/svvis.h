//
// Created by julian on 21.01.2020.
//

#ifndef DRONE_SVVIS_H
#define DRONE_SVVIS_H

#include "stm32f1xx_hal.h"

int svvis_get_char (UART_HandleTypeDef * uart, char* output);
void svvis_send_string (UART_HandleTypeDef * uart, char* value);
void svvis_send_short  (UART_HandleTypeDef * uart, short value, int channel);
void svvis_send_float  (UART_HandleTypeDef * uart, float value, int channel);

#endif //SVVIS_H
