//
// Created by julian on 10.01.2020.
//

#ifndef DRONE_UTIL_H
#define DRONE_UTIL_H

float convert_range(float input, float inmin, float inmax, float outmin, float outmax);
float clamp_value(float input, float min, float max);

#endif //DRONE_UTIL_H
