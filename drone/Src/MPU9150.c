// Julian Kandlhofer, 16. 10. 2019
// Implementations for MPU9150.h
// See header for more information

#include "MPU9150.h"

mpu_type mpu;


int mpu_init(I2C_HandleTypeDef *i2c, mpu_bandwidth_dlpf dlpf) {
	uint8_t data;
	int status = 0;

	mpu.pitch = 0;
	mpu.bank = 0;
	mpu.yaw = 0;

	mpu.gyro_sens = 0;
	mpu.acc_sens = 0;

	mpu.gyro.x = 0;
	mpu.gyro.y = 0;
	mpu.gyro.z = 0;

	mpu.accel = mpu.gyro;
	mpu.mag = mpu.gyro;

	mpu.temperature = 0;

  // Clock: PLL X axis gyro, recommended for stability
  // Temp enabled
  // sleep and cycle disabled
	data=0x1;
	status |= HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_PWR_MGMT_1, 1, &data, 1, HAL_MAX_DELAY);

	// set dlpf config
	data= dlpf;
  status |= HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_CONFIG, 1, &data, 1, HAL_MAX_DELAY);

	// set sample rate division
	// gyro output rate: dlpf off = 8kHz, dlpf on = 1kHz
	// sample rate = gyro output rate / (1+ sample rate divider)
	data=0x0;
  status |= HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SMPLRT_DIV, 1, &data, 1, HAL_MAX_DELAY);
	return status;
}

int mpu_i2c_bypass(I2C_HandleTypeDef *i2c, int enable) {
  uint8_t data;
  HAL_I2C_Mem_Read(i2c, MPU_I2C_ADDR, MPU9150_USER_CTRL_REG, 1, &data, 1, HAL_MAX_DELAY);

  if (enable) {
    // disable i2c master
    data &= ~(MPU_BIT_I2C_MASTER_EN);
    HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_USER_CTRL_REG, 1, &data, 1, HAL_MAX_DELAY);
    HAL_Delay(1);
    // enable i2c bypass
    data = MPU_BIT_I2C_BYPASS_EN;
    HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_INT_PIN_CFG_REG, 1, &data, 1, HAL_MAX_DELAY);
  }
  else {
    // enable i2c master
    data |= MPU_BIT_I2C_MASTER_EN;
    HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_USER_CTRL_REG, 1, &data, 1, HAL_MAX_DELAY);
    HAL_Delay(1);
    // disable i2c bypass
    data = 0;
    HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_INT_PIN_CFG_REG, 1, &data, 1, HAL_MAX_DELAY);
  }
}

int mpu_accel_config(I2C_HandleTypeDef *i2c, mpu_acc_measurement_range range) {
	int status;
	uint8_t data;

	data=(range << 3u);
  status |= HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_ACCEL_CONFIG, 1, &data, 1, HAL_MAX_DELAY);
	mpu.acc_sens = ACC_SENSITIVITY / (float)(0x1u << range);	/* calculate LSB/g */
	return status;
}

int mpu_gyro_config(I2C_HandleTypeDef *i2c, mpu_gyro_measurement_range range) {
	int status;
	uint8_t data;

	data=(range << 3u);
  status |= HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_GYRO_CONFIG, 1, &data, 1, HAL_MAX_DELAY);
  mpu.gyro_sens = GYRO_SENSITIVITY / (float)(0x1u << range);	/* calculate LSB per �/sec*/
	return status;
}


int mpu_mag_config(I2C_HandleTypeDef *i2c) {
  // Magnetometer configuration seems to be a complete shitshow.
  // https://github.com/RIOT-OS/RIOT/tree/master/drivers/mpu9x50
  // mpu automatically reads magnetometer values and stores them internally

  uint8_t data[3];

  /* Enable Bypass Mode to speak to compass directly */
  mpu_i2c_bypass(i2c, 1);

  /* Check whether compass answers correctly */
  HAL_I2C_Mem_Read(i2c, MPU_MAG_ADDR, MPU9150_COMP_WHOAMI_REG, 1, data, 1, HAL_MAX_DELAY);
  if (data[0] != MPU9150_COMP_WHOAMI_ANSWER) {
    mpu_i2c_bypass(i2c, 0);
    return -1;
  }

  /* Configure Power Down mode */
  data[0] = MPU9150_COMP_POWER_DOWN;
  HAL_I2C_Mem_Write(i2c, MPU_MAG_ADDR, MPU9150_COMP_CNTL_REG, 1, data, 1, HAL_MAX_DELAY);
  HAL_Delay(1);
  /* Configure Fuse ROM access */
  data[0]  = MPU9150_COMP_FUSE_ROM;
  HAL_I2C_Mem_Write(i2c, MPU_MAG_ADDR, MPU9150_COMP_CNTL_REG, 1, data, 1, HAL_MAX_DELAY);
  HAL_Delay(1);
  /* Read sensitivity adjustment values from Fuse ROM */
  HAL_I2C_Mem_Read(i2c, MPU_MAG_ADDR, MPU9150_COMP_ASAX_REG, 1, data, 3, HAL_MAX_DELAY);
  mpu.mag_sens.x = data[0];
  mpu.mag_sens.y = data[1];
  mpu.mag_sens.z = data[2];

  /* Configure Power Down mode again */
  data[0] = MPU9150_COMP_POWER_DOWN;
  HAL_I2C_Mem_Write(i2c, MPU_MAG_ADDR, MPU9150_COMP_CNTL_REG, 1, data, 1, HAL_MAX_DELAY);
  HAL_Delay(1);

  /* Disable Bypass Mode to configure MPU as master to the compass */
  mpu_i2c_bypass(i2c, 0);

  /* Configure MPU9150 for single master mode */
  data[0] = BIT_WAIT_FOR_ES;
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_I2C_MST_REG, 1, data, 1, HAL_MAX_DELAY);

  /* Set up slave line 0 */
  /* Slave line 0 reads the compass data */
  data[0] = (BIT_SLAVE_RW | MPU_MAG_ADDR);
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE0_ADDR_REG, 1, data, 1, HAL_MAX_DELAY);

  /* Slave line 0 read starts at compass data register */
  data[0] = COMPASS_DATA_START_REG;
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE0_REG_REG, 1, data, 1, HAL_MAX_DELAY);
  /* Enable slave line 0 and configure read length to 6 consecutive registers */
  data[0] = (BIT_SLAVE_EN | 0x06);
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE0_CTRL_REG, 1, data, 1, HAL_MAX_DELAY);

  /* Set up slave line 1 */
  /* Slave line 1 writes to the compass */
  data[0] = MPU_MAG_ADDR;
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE1_CTRL_REG, 1, data, 1, HAL_MAX_DELAY);

  /* Slave line 1 write starts at compass control register */
  data[0] = COMPASS_CNTL_REG;
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE1_REG_REG, 1, data, 1, HAL_MAX_DELAY);

  /* Enable slave line 1 and configure write length to 1 register */
  data[0] =  (BIT_SLAVE_EN | 0x01);
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE1_CTRL_REG, 1, data, 1, HAL_MAX_DELAY);
  /* Configure data which is written by slave line 1 to compass control */
  data[0] =  MPU9150_COMP_SINGLE_MEASURE;
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_SLAVE1_DATA_OUT_REG, 1, data, 1, HAL_MAX_DELAY);
  /* Slave line 0 and 1 operate at each sample */
  data[0] =  (BIT_SLV0_DELAY_EN | BIT_SLV1_DELAY_EN);
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_I2C_DELAY_CTRL_REG, 1, data, 1, HAL_MAX_DELAY);

  /* Set I2C bus to VDD */
  data[0] =  BIT_I2C_MST_VDDIO;
  HAL_I2C_Mem_Write(i2c, MPU_I2C_ADDR, MPU9150_YG_OFFS_TC_REG, 1, data, 1, HAL_MAX_DELAY);

  return 0;
}

int mpu_read_acc_xyz(I2C_HandleTypeDef *i2c) {
	uint8_t i2cBuffer[10];
	uint8_t help;
	int status;

	status = HAL_I2C_Mem_Read(i2c, MPU_I2C_ADDR, MPU9150_ACCEL_XOUT_H, 1, i2cBuffer, 6, HAL_MAX_DELAY);

  help = bytes_to_u16(i2cBuffer[0],i2cBuffer[1]);
  mpu.accel.x = ((float)help)/ mpu.acc_sens;

  help = bytes_to_u16(i2cBuffer[2],i2cBuffer[3]);
  mpu.accel.y = ((float)help)/ mpu.acc_sens;

  help = bytes_to_u16(i2cBuffer[4],i2cBuffer[5]);
  mpu.accel.z = ((float)help)/ mpu.acc_sens;

  return status;
}

int mpu_read_gyro_xyz(I2C_HandleTypeDef *i2c) {
	uint8_t i2cBuffer[10];
	short int help;
	int status;

  status = HAL_I2C_Mem_Read(i2c, MPU_I2C_ADDR, MPU9150_GYRO_XOUT_H, 1, i2cBuffer, 6, HAL_MAX_DELAY);

  help = bytes_to_u16(i2cBuffer[0],i2cBuffer[1]);
  mpu.gyro.x = ((float)help)/ mpu.gyro_sens;

  help = bytes_to_u16(i2cBuffer[2],i2cBuffer[3]);
  mpu.gyro.y = ((float)help)/ mpu.gyro_sens;

  help = bytes_to_u16(i2cBuffer[4],i2cBuffer[5]);
  mpu.gyro.z = ((float)help)/ mpu.gyro_sens;

  return status;
}

int mpu_read_mag_xyz(I2C_HandleTypeDef *i2c) {
  uint8_t i2cBuffer[6];
  int status;
	status = HAL_I2C_Mem_Read(i2c, MPU_I2C_ADDR, MPU9150_EXT_SENS_DATA_START_REG, 1, i2cBuffer, 6, HAL_MAX_DELAY);

  mpu.mag.x = bytes_to_u16(i2cBuffer[0],i2cBuffer[1]);
  mpu.mag.y = bytes_to_u16(i2cBuffer[2],i2cBuffer[3]);
  mpu.mag.z = bytes_to_u16(i2cBuffer[4],i2cBuffer[5]);

  /* Compute sensitivity adjustment */
  mpu.mag.x = (float) (((float)mpu.mag.x) *
                              ((((mpu.mag_sens.x - 128) * 0.5) / 128.0) + 1));
  mpu.mag.y = (float) (((float)mpu.mag.y) *
                              ((((mpu.mag_sens.y - 128) * 0.5) / 128.0) + 1));
  mpu.mag.z = (float) (((float)mpu.mag.z) *
                              ((((mpu.mag_sens.z - 128) * 0.5) / 128.0) + 1));

  /* Normalize data according to full-scale range */
  mpu.gyro.x *= 0.3f;
  mpu.gyro.y *= 0.3f;
  mpu.gyro.z *= 0.3f;

  return status;
}

int mpu_read_all(I2C_HandleTypeDef *i2c) {
	uint8_t i2cBuffer[20];
	int16_t help;
	int status;

	status = HAL_I2C_Mem_Read(i2c, MPU_I2C_ADDR, MPU9150_ACCEL_XOUT_H, 1, i2cBuffer, 14, HAL_MAX_DELAY);

	// Accelerometer XYZ
  help = bytes_to_u16(i2cBuffer[0],i2cBuffer[1]);
	mpu.accel.x= ((float)help)/ mpu.acc_sens;

  help = bytes_to_u16(i2cBuffer[2],i2cBuffer[3]);
	mpu.accel.y = ((float)help)/ mpu.acc_sens;

  help = bytes_to_u16(i2cBuffer[4],i2cBuffer[5]);
	mpu.accel.z = ((float)help)/ mpu.acc_sens;

	// Temperature sensor
  help = bytes_to_u16(i2cBuffer[6],i2cBuffer[7]);
  mpu.temperature = ((float)help/340) + 35;

	// Gyro XYZ
  help = bytes_to_u16(i2cBuffer[8],i2cBuffer[9]);
	mpu.gyro.x = ((float)help)/ mpu.gyro_sens;

  help = bytes_to_u16(i2cBuffer[10],i2cBuffer[11]);
	mpu.gyro.y = ((float)help)/ mpu.gyro_sens;

  help = bytes_to_u16(i2cBuffer[12],i2cBuffer[13]);
	mpu.gyro.z = ((float)help)/ mpu.gyro_sens;

//	status |= mpu_read_mag_xyz(i2c);
	
	return status;
}

int mpu_complementaryFilter(void) {
  static const double sampling_time = SYSTICK_TIME * 0.001;
  static float acc_pitch_angle = 0, acc_bank_angle = 0;

  // sensor is upside down
  mpu.accel.z = -mpu.accel.z;
  // convert gravity acceleration to angle
  if (mpu.accel.z != 0) {
    acc_pitch_angle = RAD_TO_DEG * atan(mpu.accel.x / mpu.accel.z);
    acc_bank_angle = -(RAD_TO_DEG * atan(mpu.accel.y / mpu.accel.z));
  }

  // Apply Complementary Filter
  mpu.pitch = COMP_GYRO_COEFF * (mpu.pitch + (mpu.gyro.y * sampling_time)) + (1 - COMP_GYRO_COEFF) * acc_pitch_angle;
  mpu.bank = COMP_GYRO_COEFF * (mpu.bank + (mpu.gyro.x * sampling_time)) + (1 - COMP_GYRO_COEFF) * acc_bank_angle;

  // TODO: Calculate YAW with magnetometer + gyro
//  mpu.yaw = COMP_GYRO_COEFF * (mpu.yaw + (mpu.gyro.z * sampling_time)) + (1 - COMP_GYRO_COEFF) * ???;
}
