//
// Created by julian on 11.10.2019.
//

#include "controller.h"

extern I2C_HandleTypeDef hi2c1;
extern ADC_HandleTypeDef hadc1;

controller_type controller;

void controller_init(void) {
  float kp = 0.48f;
  float ki = 0.001f;
  float kd = 0.005f;

//  float kp = 0.3f;
//  float ki = 0.05f;
//  float kd = 0.0f;
  float max = 100;
  pid_init(&controller.pitch_pid, kp, ki, kd, max);
  pid_init(&controller.bank_pid,  kp, ki, kd, max);
}

void controller_loop(void) {
  // Get vehicle attitude
  mpu_read_all(&hi2c1);
  mpu_complementaryFilter();

  // Get setpoints
  receiver_calculateInput();

  // Kill engines if switch is flipped or receiver signal is invalid
  if(receiver.killSwitch == 0 || receiver.invalid > 0) {
    engine.state = ENGINE_KILL;

    // reset pid when engines are killed
    // stops integrator from winding up on uneven ground.
    controller_init();
  }
  else {
    engine.state = ENGINE_NORMAL;
  }

  controller.bank_set  = receiver.invalid == 0 ? receiver.input.bank : 0;
  controller.pitch_set = receiver.invalid == 0 ? receiver.input.pitch : 0;
  controller.yaw_set = receiver.invalid == 0 ? receiver.input.yaw : 0;
  // calculate error
  float bank_error = controller.bank_set - mpu.bank;
  float pitch_error = controller.pitch_set - mpu.pitch;

  // Calculate new control output
  controller.bank_out  = pid_calculate(&controller.bank_pid, bank_error);
  controller.pitch_out = pid_calculate(&controller.pitch_pid, pitch_error);
  controller.yaw_out = controller.yaw_set;
  controller.thrust_out = receiver.input.power;


  // Motor Algorithm
  // Calculates power of each Engine based on the control output
  engine.power.front_right =
    controller.thrust_out + controller.pitch_out + controller.yaw_out - controller.bank_out;
  engine.power.front_left  =
    controller.thrust_out + controller.pitch_out - controller.yaw_out + controller.bank_out;
  engine.power.back_left   =
    controller.thrust_out - controller.pitch_out + controller.yaw_out + controller.bank_out;
  engine.power.back_right  =
    controller.thrust_out - controller.pitch_out - controller.yaw_out - controller.bank_out;

  engines_output();
}
