//
// Julian Kandlhofer, 5BHEL 13.10.2019
//


#include "util.h"
#include "engines.h"

extern TIM_HandleTypeDef htim3;
engine_type engine;

void engines_init() {
  // enable output compare
  // doing this with hal doesn't work for some reason
  htim3.Instance->CCER |= (0x1) | (0x1 << 4) | (0x1 << 8) | (0x1 << 12);

  for(int i = 0; i<4; i++)
    engine.power.dir[i] = 0;

  engine.state = ENGINE_KILL;

  engines_output();
}

void engines_output() {
  // convert % to timer ticks
  float output = 0;
  for(int i = 0; i < 4; i++) {
    //  Engine idle thrust
    //  If engines are killed, power should remain 0
    //  If engines are active power from 0-100% should move
    //  between idle thrust and 100% thrust
    switch(engine.state) {
      case ENGINE_KILL:
        // dont turn on engines
        output = 0;
        break;

      case ENGINE_NORMAL:
        // make 0% = IDLE power
        //      100% = 100% power
        // so rotors never stop
        output = convert_range(
                engine.power.dir[i],
                0.f,
                100.f,
                ENGINE_IDLE_POWER,
                100.f
        );
        // limit output from IDLE to 100%
        output = clamp_value(output, ENGINE_IDLE_POWER, 100.f);
        break;
    }

    engine.out.dir[i] = output;
    // output pwm signal
    // converts 0 to 100% to 1000 to 2000 ticks
    // equaling 1 to 2ms pulse width
    *(&(htim3.Instance->CCR1) + i) =(int)(1000 + (output * 10.0f));
  }
}

void engines_stop() {
  engine.state = ENGINE_KILL;
  engines_output();
}