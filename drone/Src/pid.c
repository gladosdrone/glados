// Julian Kandlhofer, 2019-2020
// PID-Library

#include "pid.h"

float pid_calculate(pid_type * pid, float error) {
  // Integrate Error
  // Anti Windup
  // Stop integrating when the previous ouput was out of bounds
  if((-pid->max < pid->u) && (pid->u < pid->max)) {
    pid->esum += error * DELTA_T;
  }

  // Calculate Components
  float p = pid->p_term * error;
  float i = pid->i_term * pid->esum;
  float d = pid->d_term * (error - pid->elast);

  // store output
  pid->u = p + i + d;
  // store last error
  pid->elast = error;

  // return output
  return pid->u;
}

void pid_updateTerms(pid_type * pid) {
  pid->p_term = pid->kp;
  pid->i_term = pid->ki;
  pid->d_term = pid->kd / DELTA_T;
}

void pid_init(pid_type * pid, float kp, float ki, float kd, float max) {
  pid->kp = kp;
  pid->ki = ki;
  pid->kd = kd;
  pid->max = max;

  pid->elast = 0;
  pid->esum = 0;
  pid->u = 0;

  pid_updateTerms(pid);
}

