// Turnigy X9 Receiver Library
// Julian Kandlhofer 12.10.2019
//
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_tim.h>
#include <string.h>
#include "svvis.h"
#include "receiver.h"
#include "util.h"

extern TIM_HandleTypeDef htim4;

receiver_type receiver;

#define THRESHOLD(value, threshold) (value > -threshold && value < threshold ? 0.0f : value)

void receiver_init() {
  // disable counter
  htim4.Instance->CR1 &= ~0x1;

  // enable update event
  htim4.Instance->CR1 &= ~(0x1<<1);

  // enable update and capture ch 4 interupt
  htim4.Instance->DIER |= (0x1) | (0x1<<4);

  // IC4 connected to TI4
  htim4.Instance->CCMR2 &= ~(0x11 <<8);
  htim4.Instance->CCMR2 |= (0x01 <<8);
  // enabe cch4, falling edge
  htim4.Instance->CCER |= 0x11 << 12;

  // enable counter
  htim4.Instance->CR1 |= 0x1;

  memset(receiver.pulse_width, TPULSE_MIN, 8);

  receiver.invalid = 1;
  receiver.killSwitch = 0;

}

void receiver_getChannels() {
  static int currentChannel = 0;
  static int measuredTime = 0;

  // Check for Capture interrupt
  if (__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC4)) {
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_CC4);
    measuredTime = htim4.Instance->CCR4;
    // Reset Counter
    htim4.Instance->CCR4 = 0;
    htim4.Instance->CNT = 0;

    // Signal Longer than max sync pulse, must be invalid
    if(measuredTime > TMEAS_MAX ) {
      receiver.invalid = 1;
      currentChannel = 0;
      return;
    }

    // Longer than expected time between pulses
    // Must be sync-pulse. start reading values
    if(measuredTime > TNEXT) {
      receiver.invalid = 0;
      currentChannel = 0;
      return;
    }

    receiver.pulse_width[currentChannel++] = measuredTime - TGAP + TDELAY;
  }
  // Update Event aka Overflow
  if (__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_UPDATE)) {
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_UPDATE);
    memset(receiver.pulse_width, TPULSE_MIN, 8);
    receiver.invalid = 2;
    currentChannel = 0;
  }
}

void receiver_calculateInput(void) {
  float temp;
  for(int channel = 0; channel < 5; channel++) {
    // convert pulse width to 0 to 100%
    temp = convert_range(receiver.pulse_width[channel], TPULSE_MIN, TPULSE_MAX, 0, 100);

    // clamp values between 0 and 100%
    temp = clamp_value(temp, 0, 100);

    // convert percent into setpoint values
    switch (channel + 1) {
      // Bank / Pitch
      // convert to target angle +/- [°]
      // ignore under 1°
      case 1:
      case 2:
        temp = (temp * 2* MAXANGLE/100) - MAXANGLE;
        temp = THRESHOLD(temp, 1.0f);
        break;

      // Power
      // keep as %
      // don't turn on under 5%
      case 3:
        temp = THRESHOLD(temp, 5.0f);
        break;

      // Yaw
      // convert to rotation rate +/-50°/s
      // ignore under 1°/s
      case 4:
        temp -= 50.0f;
        temp /= 5;
        temp = THRESHOLD(temp, 1.0f);
        break;

      // Channel 5 is the switch
      case 5:
        receiver.killSwitch = temp > 50.f;
        break;

      case 6:
        receiver.resetSwitch = temp > 50.f;
        break;

    }

    receiver.input.channel[channel] = temp;

  }
}

void receiver_sendChannelStatus(UART_HandleTypeDef *huart) {
  if(receiver.invalid)
    svvis_send_string(huart, "Receiver Invalid!");
}