//
// Created by julian on 21.01.2020.
//

#include "svvis.h"
#include <string.h>

static void send_byte(UART_HandleTypeDef * uart, uint8_t data) {
  HAL_UART_Transmit(uart, &data, 1, HAL_MAX_DELAY);
}

void svvis_send_string(UART_HandleTypeDef *uart, char *value) {
  send_byte(uart, 10);
  HAL_UART_Transmit(uart, (uint8_t*)value, strlen(value), HAL_MAX_DELAY);
  send_byte(uart, '\0');
}

void svvis_send_short(UART_HandleTypeDef *uart, short value, int channel) {
  send_byte(uart, 10 + channel);
  HAL_UART_Transmit(uart, (uint8_t*)&value, 2, HAL_MAX_DELAY);
}

void svvis_send_float(UART_HandleTypeDef *uart, float value, int channel) {
  send_byte(uart, 20 + channel);
  HAL_UART_Transmit(uart, (uint8_t*)&value, 4, HAL_MAX_DELAY);
}

int svvis_get_char(UART_HandleTypeDef *uart, char *output) {
  // check if data has been received
  if(uart->Instance->SR & USART_SR_RXNE) {
    HAL_UART_Receive(uart, (uint8_t*)output, 1, HAL_MAX_DELAY);
    return 0;
  }
  return 1;
}
