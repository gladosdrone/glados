//
// Created by julian on 10.01.2020.
//

#include "util.h"

float convert_range(float input, float inmin, float inmax, float outmin, float outmax) {
  return (((input - inmin) * (outmax - outmin)) / (inmax - inmin)) + outmin;
}

float clamp_value(float input, float min, float max) {
  if(input > max)
    return max;

  if(input < min)
    return min;

  return input;
}
