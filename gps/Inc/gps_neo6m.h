#ifndef GPS_GPS_NEO6M_H
#define GPS_GPS_NEO6M_H

#include "stm32f1xx_hal.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef EVA_MAKE_LIB
#define EXPORT
#else
#define EXPORT extern
#endif

/*! \file */

/** Error return values
 */
enum {
    E_OK,       /**< No error */
    E_NO_MEM,   /**< Out of memory */
    E_GENERIC,  /**< Generic error */
};

/** Data status
 */
typedef enum {
    S_NONE=0,       /**< None */
    S_OK='A',       /**< Data valid */
    S_WARNING='V'   /**< Navigation receiver warning */
} gpstatus_t;

/** Mode indicator
*/
typedef enum {
    M_NONE=0,       /**< None */
    M_AUTO='A',     /**< Autonomous mode */
    M_DIFF='D',     /**< Differential mode */
    M_ESTI='E',     /**< Estimated (dead reckoning) mode */
    M_MANU='M',     /**< Manual input mode */
    M_SIMU='S',     /**< Simulator mode */
    M_INVA='N'      /**< Data not valid */
} gpmode_t;

/** Direction
*/
typedef enum {
    D_NONE=0,       /**< None */
    D_NORTH='N',    /**< North */
    D_EAST='E',     /**< East */
    D_SOUTH='S',    /**< South */
    D_WEST='W'      /**< West */
} gpdir_t;

/** GPS Quality Indicator
*/
typedef enum {
    Q_INVALID=0,      /**< Invalid */
    Q_GPSSPS=1,       /**< GPS SPS mode, fix valid */
    Q_DIFSPS=2,       /**< Differential GPS, SPS mode, fix valid */
    Q_GPSPPS=3,       /**< GPS PPS mode, fix valid */
    Q_RTK=4,          /**< Real Time Kinematic mode, fixed integers */
    Q_FRTK=5,         /**< Real Time Kinematic mode, floats */
    Q_ESTIMATED=6,    /**< Estimated (dead reckoning) mode */
    Q_MANUAL=7,       /**< Manual input mode */
    Q_SIMULATOR=8     /**< Simulator mode */
} gpqual_t;

/** GPS Fix mode
*/
typedef enum {
    MODE_NONE=0,       /**< None */
    M_NOFIX,        /**< Fix not availabe */
    M_2D,           /**< 2D Fix */
    M_3D            /**< 3D Fix */
} gpfmode_t;

/** Time
*/
typedef struct gptime_t {
    unsigned char h;    /**< Hours */
    unsigned char m;    /**< Minutes */
    unsigned char s;    /**< Seconds */
    unsigned int ms;    /**< Milliseconds */
} gptime_t;

/** Date
*/
typedef struct gpdate_t {
    unsigned char d;    /**< Day */
    unsigned char m;    /**< Month */
    unsigned int y;     /**< Year */
} gpdate_t;

/** Latitude
*/
typedef struct gplat_t {
    unsigned char deg;  /**< Degrees */
    float min;          /**< Minutes */
    gpdir_t dir;        /**< Direction */
} gplat_t;

/** Longitude
*/
typedef struct gplon_t {
    unsigned char deg;  /**< Degrees */
    float min;          /**< Minutes */
    gpdir_t dir;        /**< Direction */
} gplon_t;

/** Magnetic variation
*/
typedef struct gpmagvar_t {
    float variation;    /**< Variation in degrees */
    gpdir_t dir;        /**< Direction */
} gpmagvar_t;

/** Sattelite information
*/
typedef struct gpsv_t {
    unsigned char id;           /**< Sattelite ID number */
    unsigned char elevation;    /**< Elevation in degrees, max 90° */
    unsigned int azimuth;       /**< Azimuth in true degrees, 0° - 359° */
    unsigned char snr;          /**< Signal Noise Ration, 0 - 99 dB */
} gpsv_t;

/** TXT: Text transmission record
*
* The TXT record is intended for short text messages, longer messages
* can be transmitted using multiple sentences. The #current_sentence number
* is used to define the order of the sentences. The identifier indentifies
* the message. The text may consist of up to 61 ASCII characters or control
* codes.
*
* An example text for this record could be:
*
* <tt>ROM CORE 7.03 (45969) Mar 17 2011 16:18:34</tt>
*
* \note A #text can contain the special sequence \c ^NN where NN stands for an ASCII characters hex code.
*       This is used to encode reserved characters that may not be used in the message. For example
*       to transmit the \c ^ character, the following sequence would be used: \c ^5E
*
* \latexonly
* \pagebreak
* \endlatexonly
*
* The reserved characters in the NMEA protocol are the following:
*
* <table>
*   <caption id="multi_row">Reserved Characters</caption>
*   <tr><th>Character         <th>Hex          <th>Dec    <th>Notes
*   <tr><td>\c !              <td>\c 0x21      <td>33     <td>Encapsulation sentence start
*   <tr><td>\c $              <td>\c 0x24      <td>36     <td>Parametric sentence start
*   <tr><td>\c *              <td>\c 0x2A      <td>42     <td>Checksum start
*   <tr><td>\c ,              <td>\c 0x2C      <td>44     <td>Field delimiter
*   <tr><td>\c ^              <td>\c 0x5E      <td>94     <td>Hex escape sequence start
*   <tr><td>\c \\             <td>\c 0x5C      <td>92     <td>
*   <tr><td>\c ~              <td>\c 0x7E      <td>126    <td>
*   <tr><td><em>[CR]</em>     <td>\c 0x0D      <td>13     <td>
*   <tr><td><em>[LF]</em>     <td>\c 0x0A      <td>10     <td>
*   <tr><td><em>[DEL]</em>    <td>\c 0x7F      <td>127    <td>
* </table>
*
*/
typedef struct gptxt_t {
    unsigned char number_of_sentences;  /**< Number of sentences in the message, 1 - 99 */
    unsigned char current_sentence;     /**< Current sentence number, 1 - 99 */
    unsigned char text_identifier;      /**< Text identifier, 1 - 99 */
    char text[62];                      /**< ASCII characters and code delimiters, maximal length 61 */
} gptxt_t;

/** RMC: Recommended Minumum Specific GNSS Data record
*
* The RMC record contains the time, position, course and the speed provided by
* the GNSS receiver. It is transmitted at intervals not exceeding 2 seconds.
*
* \note All data field are provided, null fields are used when data is currently
*       unavailable.
*/
typedef struct gprmc_t {
    gptime_t time;          /**< UTC time of position fix */
    gpstatus_t status;      /**< Data status */
    gplat_t latitude;       /**< Latitude */
    gplon_t longitude;      /**< Longitude */
    float speed;            /**< Speed over ground in knots */
    float course;           /**< Course over ground in true degrees */
    gpdate_t date;          /**< Date */
    gpmagvar_t variation;   /**< Magnetic variation */
    gpmode_t mode;          /**< Mode indicator */
} gprmc_t;

/** VTG: Course Over Ground and Ground Speed record
*
* The VTG record contains the course and speed realtive to the ground.
*/
typedef struct gpvtg_t {
    float course_true;      /**< Course over ground in true degrees */
    float course_magnetic;  /**< Course over ground in magnetic degrees */
    float speed_knots;      /**< Speed over ground in knots */
    float speed_kmh;        /**< Speed over ground in km per hour */
    gpmode_t mode;          /**< Mode indicator */
} gpvtg_t;

/** GGA: Global Positioning System Fix Data record
*
* The GGA record contains time, position and fix related data for the GPS
* receiver.
* The #geoidal_separation contains the difference WGS-48 earth ellipsoid surface
* and mean-sea-level, if negative that indicates that the mean-sea-level is below
* the WGS-48 earth ellisoid surface.
*/
typedef struct gpgga_t {
    gptime_t time;                  /**< UTC time of position fix */
    gplat_t latitude;               /**< Latitude */
    gplon_t longitude;              /**< Longitude */
    gpqual_t quality;               /**< GPS Quality indicator */
    unsigned char satellites_used;  /**< Number of sattelites in use, 0 - 12 */
    float horizontal_dilution;      /**< Horizontal dilution of precision */
    float altitude;                 /**< Altitude regarding mean-sea-level (geoid) in meters */
    float geoidal_separation;       /**< Geoidal separation in meters */
    float diff_age;                 /**< Age of differential GPS data */
    unsigned int diff_id;           /**< Differential reference station ID, 0 - 1023 */
} gpgga_t;

/** GSA: GNSS DOP and Active Sattelites record
*
* The GSA record contains information about the GNSS receiver mode, the sattelites
* used and the dilusion of precision (DOP) values. Each GSA record has an array (#id)
* which contains the ids of the sattelites used. The #pdop, #hdop and #vdop values
* are for the combined sattelites used.
*
* Sattelite ID numers are divided into different groups, depending on the sattelite
* system they are part of.
*
* <table>
*   <caption id="multi_row">Sattelite ID ranges</caption>
*   <tr><th>IDs               <th>Sattelite system       <th>Notes
*   <tr><td>1 - 32            <td>GPS satellites         <td>Identified by their PRN
*   <tr><td>33 - 64           <td>WAAS sattelites        <td>To obtain the PRN for WAAS sattelites, add 87 to the ID
*   <tr><td>65 - 96           <td>GLONASS settelites     <td>To obtain the GLONASS ID, substract 64 from the ID
* </table>
*/
typedef struct gpgsa_t {
    gpmode_t mode;          /**< Mode indicator */
    gpfmode_t fix_mode;     /**< Fix mode indicator */
    unsigned char id[12];   /**< Sattelite ID numbers used in solution */
    float pdop;             /**< Positional dilution of precision (PDOP) */
    float hdop;             /**< Horizontal dilution of precision (HDOP) */
    float vdop;             /**< Vertical dilution of precision (VDOP) */
} gpgsa_t;

/** GSV: GNSS Sattelite in View record
*
* \todo Currently, the GSV record is not parsed!
*
* The GSV record includes information about the sattelites that are currently in view of the
* receiver. For each sattelite the ID number, elevation, azimuth and SNR value are included.
* For more information about the sattelite ID numbers, see the \link #gpgsa_t GSA record
* documentation\endlink.
*/
typedef struct gpgsv_t {
    unsigned char total_sentences;      /**< Total number of sentences, 1 - 9 */
    unsigned char sentence;             /**< Current sentence number, 1 - 9 */
    unsigned char satellites_in_view;   /**< Total number of sattelites in view */
    gpsv_t satellites[36];              /**< Array of sattelites information */
} gpgsv_t;

/** GLL: Geographic postion
*
* The GLL record contains the latitude, longitude, time of fix and the current status.
*/
typedef struct gpgll_t {
    gplat_t latitude;           /**< Latitude */
    gplon_t longitude;          /**< Longitude */
    gptime_t time_of_position;  /**< UTC of position */
    gpstatus_t status;          /**< GPS status */
    gpmode_t mode;              /**< Mode indicator */
} gpgll_t;


extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

/** USART1 interrupt handler.
* This is the USART1 interrupt handler called by
* the system.
*/
EXPORT void USART2_IRQHandler(void);

/** Initialize the library
* Initializes ports, USART1 and the NVIC
*/
EXPORT void init_gps(void);

/** Returns the last received \link #gptxt_t TXT record\endlink
*/
EXPORT gptxt_t get_txt(void);

/** Returns the last received \link #gprmc_t RMC record\endlink
*/
EXPORT gprmc_t get_rmc(void);

/** Returns the last received \link #gpvtg_t VTG record\endlink
*/
EXPORT gpvtg_t get_vtg(void);

/** Returns the last received \link #gpgga_t GGA record\endlink
*/
EXPORT gpgga_t get_gga(void);

/** Returns the last received \link #gpgsa_t GSA record\endlink
*/
EXPORT gpgsa_t get_gsa(void);

/** Returns the last received \link #gpgsv_t GSV record\endlink
* \todo Currently, the GSV record is not parsed!
*/
EXPORT gpgsv_t get_gsv(void);

/** Returns the last received \link #gpgll_t GLL record\endlink
*/
EXPORT gpgll_t get_gll(void);


#endif //GPS_GPS_NEO6M_H
