#include "gps_neo6m.h"

char    uart_buffer[90]      = {0};
int     uart_buffer_position = 0;
char    sentence[80];

static unsigned char ascii_to_hex_nibble(char c);
static unsigned char ascii_to_hex(char c[2]);
static unsigned char check_checksum(void);
static size_t split_string(char *string, char sep, char *components[], size_t size);
static void parse_lat(const char *ls, gplat_t *lat);
static void parse_lon(const char *ls, gplon_t *lon);
static void parse_time(const char *ts, gptime_t *time);
static void parse_date(const char *ds, gpdate_t *date);
static void parse_nmea(void);

gptxt_t txt_record;
gprmc_t rmc_record;
gpvtg_t vtg_record;
gpgga_t gga_record;
gpgsa_t gsa_record;
gpgsv_t gsv_record;
gpgll_t gll_record;

static unsigned char ascii_to_hex_nibble(char c)
{
  if (c >= 'A' && c <= 'F') {
    return c - 'A' + 10;
  } else {
    return c - '0';
  }
}

static unsigned char ascii_to_hex(char c[2])
{
  return ascii_to_hex_nibble(c[0]) * 16 + ascii_to_hex_nibble(c[1]);
}

static unsigned char check_checksum(void)
{
  unsigned char checksum = ascii_to_hex(uart_buffer + uart_buffer_position - 4);
  unsigned char i = 1;
  unsigned char val = 0x00;
  do {
    val ^= uart_buffer[i];
    sentence[i-1] = uart_buffer[i];
  } while(uart_buffer[++i] != '*');
  sentence[i-1] = '\0';
  return (val == checksum);
}

static size_t split_string(char *string, char sep, char *components[], size_t size)
{
  char *start = string;
  char *pos;
  size_t count = 0;
  pos = strchr(string, sep);
  while (pos) {
    if (size <= count + 1) {
      return 0;
    }
    *pos = '\0';
    components[count++] = start;
    start = pos + 1;
    pos = strchr(start, sep);
  }
  components[count] = start;
  return count + 1;
}

static void parse_lat(const char *ls, gplat_t *lat)
{
  char c_deg[3];
  if(ls[0] == 0) {
    lat->deg = 0;
    lat->min = 0;
  }
  strncpy(c_deg, ls, 2);
  lat->deg = atoi(c_deg);
  lat->min = atof(ls+2);
  return;
}

static void parse_lon(const char *ls, gplon_t *lon)
{
  char c_deg[4];
  if(ls[0] == 0) {
    lon->deg = 0;
    lon->min = 0;
  }
  strncpy(c_deg, ls, 3);
  lon->deg = atoi(c_deg);
  lon->min = atof(ls+3);
  return;
}

static void parse_time(const char *ts, gptime_t *time)
{
  char buf[2];
  if(ts[0] == 0) {
    time->h = 0;
    time->m = 0;
    time->s = 0;
    time->ms = 0;
    return;
  }
  strncpy(buf, ts, 2);
  time->h = atoi(buf);
  strncpy(buf, ts+2, 2);
  time->m = atoi(buf);
  strncpy(buf, ts+4, 2);
  time->s = atoi(buf);
  strncpy(buf, ts+7, 2);
  time->ms = atoi(buf);
  return;
}

static void parse_date(const char *ds, gpdate_t *date)
{
  char buf[2];
  if(ds[0] == 0) {
    date->d = 0;
    date->m = 0;
    date->y = 0;
    return;
  }
  strncpy(buf, ds, 2);
  date->d = atoi(buf);
  strncpy(buf, ds+2, 2);
  date->m = atoi(buf);
  strncpy(buf, ds+4, 2);
  date->y = atoi(buf);
  return;
}

static void parse_nmea(void)
{
  if(!strncmp(uart_buffer, "$GPTXT", 6)) {
    // TXT Record
    static char *tmp[5];
    split_string(uart_buffer, ',', tmp, 5);
    txt_record.number_of_sentences = atoi(tmp[1]);
    txt_record.current_sentence = atoi(tmp[2]);
    txt_record.text_identifier = atoi(tmp[3]);
    strcpy(txt_record.text, tmp[4]);
  } else if(!strncmp(uart_buffer, "$GPRMC", 6)) {
    // RMC Record
    static char *tmp[13];
    split_string(uart_buffer, ',', tmp, 13);
    parse_time(tmp[1], &rmc_record.time);
    rmc_record.status = (gpstatus_t)tmp[2][0];
    parse_lat(tmp[3], &rmc_record.latitude);
    rmc_record.latitude.dir = (gpdir_t)tmp[4][0];
    parse_lon(tmp[5], &rmc_record.longitude);
    rmc_record.longitude.dir = (gpdir_t)tmp[6][0];
    rmc_record.speed = atof(tmp[7]);
    rmc_record.course = atof(tmp[8]);
    parse_date(tmp[9], &rmc_record.date);
    rmc_record.variation.variation = atof(tmp[10]);
    rmc_record.variation.dir = (gpdir_t)tmp[11][0];
    rmc_record.mode = (gpmode_t)tmp[12][0];
  } else if(!strncmp(uart_buffer, "$GPVTG", 6)) {
    // VTG Record
    static char *tmp[10];
    split_string(uart_buffer, ',', tmp, 10);
    vtg_record.course_true = atof(tmp[1]);
    vtg_record.course_magnetic = atof(tmp[3]);
    vtg_record.speed_knots = atof(tmp[5]);
    vtg_record.speed_kmh = atof(tmp[7]);
    vtg_record.mode = (gpmode_t)tmp[9][0];
  } else if(!strncmp(uart_buffer, "$GPGGA", 6)) {
    // GGA Record
    static char *tmp[15];
    split_string(uart_buffer, ',', tmp, 15);
    parse_time(tmp[1], &gga_record.time);
    parse_lat(tmp[2], &gga_record.latitude);
    gga_record.latitude.dir = (gpdir_t)tmp[3][0];
    parse_lon(tmp[4], &gga_record.longitude);
    gga_record.longitude.dir = (gpdir_t)tmp[5][0];
    gga_record.quality = (gpqual_t)atoi(tmp[6]);
    gga_record.satellites_used = atoi(tmp[7]);
    gga_record.horizontal_dilution = atof(tmp[8]);
    gga_record.altitude = atof(tmp[9]);
    gga_record.geoidal_separation = atof(tmp[11]);
    gga_record.diff_age = atof(tmp[13]);
    gga_record.diff_id = atoi(tmp[14]);
  } else if(!strncmp(uart_buffer, "$GPGSA", 6)) {
    // GSA Record
    unsigned char i;
    static char *tmp[18];
    split_string(uart_buffer, ',', tmp, 18);
    gsa_record.mode = (gpmode_t)tmp[1][0];
    gsa_record.fix_mode = (gpfmode_t)tmp[2][0];
    for(i = 0; i < 12; i++) {
      gsa_record.id[i] = atoi(tmp[3+i]);
    }
    gsa_record.pdop = atof(tmp[15]);
    gsa_record.hdop = atof(tmp[16]);
    gsa_record.vdop = atof(tmp[17]);
  } else if(!strncmp(uart_buffer, "$GPGSV", 6)) {
    // GSV Record -- bleibt vorerst leer
  } else if(!strncmp(uart_buffer, "$GPGLL", 6)) {
    // GLL Record
    static char *tmp[8];
    split_string(uart_buffer, ',', tmp, 8);
    parse_lat(tmp[1], &rmc_record.latitude);
    gll_record.latitude.dir = (gpdir_t)tmp[2][0];
    parse_lon(tmp[3], &rmc_record.longitude);
    gll_record.longitude.dir = (gpdir_t)tmp[4][0];
    parse_time(tmp[5], &gll_record.time_of_position);
    gll_record.status = (gpstatus_t)tmp[6][0];
    gll_record.mode = (gpmode_t)tmp[7][0];
  }
}

void USART2_IRQHandler(void)
{
  char buffer;
  USART2->CR1 &= ~(0x0020);

  HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);

  while (buffer) {
    if (buffer == '$' || buffer == '!') { // Beginning of NMEA sentence
      uart_buffer_position = 0; //Set write position to 0
      uart_buffer[uart_buffer_position++] = buffer; // Write UART value into buffer
    } else if (buffer == '\xA') { // End of NMEA sentence
      uart_buffer[uart_buffer_position++] = buffer;
      uart_buffer[uart_buffer_position] = '\0'; // End String with \0
      if (uart_buffer[0] != '$' && uart_buffer[0] != '!')	{ // Invalid NMEA Sentence?
        //Reset buffer
        uart_buffer_position = 0;
        uart_buffer[0] = '\0';
      } else if (check_checksum()) { // If valid -> parse NMEA sentence
        parse_nmea();
      }
      //Reset buffer
      uart_buffer_position = 0;
      uart_buffer[uart_buffer_position] = '\0';
      break;
    } else { // If its not the end of the NMEA Sentence
      uart_buffer[uart_buffer_position++] = buffer; // Write UART value into buffer
    }

    HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
  }
  USART2->CR1 |= 0x0020;
  return;
}

void init_gps(void)
{

  NVIC->IP[USART2_IRQn] = (2<<4); // Set interrupt priority
  NVIC->ICPR[USART2_IRQn >> 0x05] |= (0x01 << (USART2_IRQn & 0x1F)); //Clear Pending Bit
  NVIC->ISER[USART2_IRQn >> 0x05] |= (0x01 << (USART2_IRQn & 0x1F)); //Enable Interrupt
  USART2->CR1 |= 0x0020; //Enable UART RXNEIE Interrupt

  return;
}

gptxt_t get_txt(void)
{
  return txt_record;
}

gprmc_t get_rmc(void)
{
  return rmc_record;
}

gpvtg_t get_vtg(void)
{
  return vtg_record;
}

gpgga_t get_gga(void)
{
  return gga_record;
}

gpgsa_t get_gsa(void)
{
  return gsa_record;
}

gpgsv_t get_gsv(void)
{
  return gsv_record;
}

gpgll_t get_gll(void)
{
  return gll_record;
}
