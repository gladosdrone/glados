#ifndef TFMINIPLUS_TFMINIPLUS_H
#define TFMINIPLUS_TFMINIPLUS_H


#include <string.h>
#include "main.h"

#define TFMP_FRAME_SIZE         9   // Size of data frame = 9 bytes
#define TFMP_REPLY_SIZE         8   // Longest command reply = 8 bytes
#define TFMP_CMD_COUNT         18   // Amount of commands

#define TFMP_DEFAULT_ADDRESS   0x10   // default I2C slave address

//internal measurement rate in Hex (Little Endian)
#define    TFMP_FRAME_0            0x0000
#define    TFMP_FRAME_1            0x0001
#define    TFMP_FRAME_2            0x0002
#define    TFMP_FRAME_5            0x0003
#define    TFMP_FRAME_10           0x000A
#define    TFMP_FRAME_20           0x0014
#define    TFMP_FRAME_25           0x0019
#define    TFMP_FRAME_50           0x0032
#define    TFMP_FRAME_100          0x0064
#define    TFMP_FRAME_125          0x007D
#define    TFMP_FRAME_200          0x00C8
#define    TFMP_FRAME_250          0x00FA
#define    TFMP_FRAME_500          0x01F4
#define    TFMP_FRAME_1000         0x03E8

typedef struct{

    //TFmini Plus I2C Address
    uint8_t i2c_address;

    //Sensor Data
    uint16_t  distance;
    uint16_t  strength;
    uint16_t  temperature;

    //Reply
    uint8_t reply[TFMP_REPLY_SIZE];

}tfmp_type;

// Command names with respective index for tfmp_cmd_frames array
enum tfmp_cmds{
    TFMP_OBTAIN_FIRMWARE_VERSION=0,
    TFMP_SYSTEM_RESET=1,
    TFMP_SET_FRAME_RATE=2,
    TFMP_TRIGGER_DETECTION=3,
    TFMP_OUTPUT_FORMAT_MM=4,
    TFMP_OUTPUT_FORMAT_CM=5,
    TFMP_OUTPUT_FORMAT_PXHW=6,
    TFMP_BAUDRATE =7,
    TFMP_ENABLE_OUTPUT =8,
    TFMP_DISABLE_OUTPUT =9,
    TFMP_INTERFACE_I2C =10,
    TFMP_INTERFACE_UART =11,
    TFMP_MODIFY_I2C_ADDRESS =12,
    TFMP_OBTAIN_DATA_FRAME_MM =13,
    TFMP_OBTAIN_DATA_FRAME_CM =14,
    TFMP_IO_MODE_ENABLE =15,
    TFMP_RESTORE_FACTORY_SETTINGS =16,
    TFMP_SAVE_SETTINGS =17
};

//Array of all commands: tfmp_cmd_frames[tfmp_cmds][data of command + reply size]
extern const uint8_t tfmp_cmd_frames[TFMP_CMD_COUNT][TFMP_FRAME_SIZE+1];


// Initializes the tfmp_type values, sets I2C address and performs a check to see if device is available
uint8_t tfmp_init(I2C_HandleTypeDef *i2c, tfmp_type *tfmp_sensor, uint8_t i2c_address);

// Sends Command to sensor and saves the response in the reply array of tfmp_sensor (waits 100ms for safety)
// For reading data it is recommended to use tfmp_getData since there is no pause between commands
uint8_t tfmp_sendCmd(I2C_HandleTypeDef *i2c, tfmp_type *tfmp_sensor, uint8_t cmd, uint32_t param);

// Gets Distance, Signal Strength and Temperature and saves the values in the respective tmfp_sensor variables
uint8_t tfmp_getData(I2C_HandleTypeDef *i2c, tfmp_type *tfmp_sensor);

// Calculates Checksum of input cmd or reply
uint8_t tfmp_calc_checksum(uint8_t *cmd, uint8_t cmd_size);

#endif //TFMINIPLUS_TFMINIPLUS_H
