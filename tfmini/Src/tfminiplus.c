#include "tfminiplus.h"

//Array of all commands cmd_frames[tfmp_cmds][data of command + reply size]
const uint8_t tfmp_cmd_frames[TFMP_CMD_COUNT][TFMP_FRAME_SIZE+1]={
        {0x5A, 0x04, 0x01, 0x5F, 0x07}, //Obtain firmware version
        {0x5A, 0x04, 0x02, 0x60, 0x05}, //System Reset
        {0x5A, 0x06, 0x03, 0x64, 0x00, 0xC7, 0x06}, //Frame Rate (Default here is 250Hz)
        {0x5A, 0x04, 0x04, 0x62, 0x09}, //Trigger Detection
        {0x5A, 0x05, 0x05, 0x06, 0x6A, 0x05}, //Output format mm
        {0x5A, 0x05, 0x05, 0x01, 0x65, 0x05}, //Output format cm
        {0x5A, 0x05, 0x05, 0x02, 0x66, 0x05}, //Output format pixhawk
        {0x5A, 0x08, 0x06, 0x00, 0xC2, 0x01, 0x00, 0xFF, 0x08}, //Baudrate (Default here is 115200 baud)
        {0x5A, 0x05, 0x07, 0x01, 0x67, 0x05}, //Enable Output
        {0x5A, 0x05, 0x07, 0x00, 0x66, 0x05}, //Disable Output
        {0x5A, 0x05, 0x0A, 0x01, 0x6A, 0x00}, //Interface I2C
        {0x5A, 0x05, 0x0A, 0x00, 0x69, 0x00}, //Interface UART
        {0x5A, 0x05, 0x0B, 0x10, 0x7A, 0x05}, //Modify I2C Address (Default here is 0x10)
        {0x5A, 0x05, 0x00, 0x06, 0x65, 0x09}, //Obtain Data Frame mm
        {0x5A, 0x05, 0x00, 0x01, 0x60, 0x09}, //Obtain Data Frame cm
        {0x5A, 0x09, 0x3B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x05}, //IO Mode enable - not implemented
        {0x5A, 0x04, 0x10, 0x6E, 0x05}, //Restore Factory Settings
        {0x5A, 0x04, 0x11, 0x6F, 0x05}, //Save Settings
};

uint8_t tfmp_init(I2C_HandleTypeDef *i2c, tfmp_type *tfmp_sensor, uint8_t i2c_address){
  tfmp_sensor->distance = 0;
  tfmp_sensor->strength = 0;
  tfmp_sensor->temperature = 0;
  tfmp_sensor->i2c_address = (i2c_address<<1); //Shift to left by 1 to add R/W Byte

  memset(tfmp_sensor->reply, 0, sizeof(tfmp_sensor->reply));

  //Check if communication with sensor is possible
  if(HAL_I2C_IsDeviceReady(i2c, tfmp_sensor->i2c_address, 3, 500) != HAL_OK){
    return 1;
  }

  return 0;
}

uint8_t tfmp_sendCmd(I2C_HandleTypeDef *i2c, tfmp_type *tfmp_sensor, uint8_t cmd, uint32_t param){

  static size_t cmd_size = 0, reply_size = 0;
  uint8_t *p = (uint8_t *)&param; // Pointer to param -> p[0] points to Least Significant Byte
  uint8_t checksum=0;

  // Get Size of command frame
  cmd_size = tfmp_cmd_frames[cmd][1];

  //Create command_frame array
  uint8_t cmd_frame[cmd_size];

  //Copy command into command frame array
  for(uint8_t i=0; i <= cmd_size; i++){
    cmd_frame[i] = tfmp_cmd_frames[cmd][i];
  }

  reply_size = cmd_frame[cmd_size]; // Gets last byte of cmd[] => reply length


  switch(cmd_frame[2]){ //Depending on Command Id
    //Parameters are written to Command frame in little endian

    case 0x03: //Frame Rate
      cmd_frame[3] = p[0];
      cmd_frame[4] = p[1];
      break;
    case 0x06: //Baud Rate
      cmd_frame[3] = p[0];
      cmd_frame[4] = p[1];
      cmd_frame[5] = p[2];
      cmd_frame[6] = p[3];
      break;
    case 0x0A: // Communication Interface (I2C/UART)
      cmd_frame[3] = p[0];
      break;
    case 0x0B: // Modify I2C Slave Address
      cmd_frame[3] = p[0];
      break;
    case 0x3B: // I/O Mode enable - not implemented
      return 1;
    default: // If command doesn't have parameters
      break;
  }

  checksum = tfmp_calc_checksum(cmd_frame, cmd_size); //Checksum calculation
  cmd_frame[cmd_size-1] = checksum; // Set Checksum of Command Frame

  if(HAL_I2C_Master_Transmit(i2c, tfmp_sensor->i2c_address, cmd_frame, cmd_size, HAL_MAX_DELAY) != HAL_OK){
    return 1;
  }

  if(HAL_I2C_Master_Receive(i2c, tfmp_sensor->i2c_address, tfmp_sensor->reply, reply_size, HAL_MAX_DELAY) != HAL_OK){
    return 1;
  }

  checksum = tfmp_calc_checksum(tfmp_sensor->reply, reply_size); // Checksum calculation

  //Theoretical calculated Checksum could be greater than 0xFF but is limited to 0x00 - 0xFF (1 Byte)
  //Check if calculated checksum matches expected checksum
  if((checksum != tfmp_sensor->reply[reply_size-1])&&(checksum != 0xFF)){
    HAL_Delay(100); //Wait 100ms (for safety)
    return 1; // Checksum does not match
  }

  HAL_Delay(100); //Wait 100ms (for safety)

  return 0;
}

uint8_t tfmp_getData(I2C_HandleTypeDef *i2c, tfmp_type *tfmp_sensor){
  static uint8_t obtain_data_frame[5] = {0x5A, 0x05, 0x00, 0x01, 0x60}; // Command Frame to get Distance in cm

  // Send Command Frame to Sensor
  if(HAL_I2C_Master_Transmit(i2c, tfmp_sensor->i2c_address, obtain_data_frame, 5, HAL_MAX_DELAY) != HAL_OK){
    return 1;
  }

  //Receive measured values from Sensor
  if(HAL_I2C_Master_Receive(i2c, tfmp_sensor->i2c_address, tfmp_sensor->reply, 9, HAL_MAX_DELAY) != HAL_OK){
    return 1;
  }

  //Save measured values
  tfmp_sensor->distance = tfmp_sensor->reply[2] | (tfmp_sensor->reply[3] <<8);
  tfmp_sensor->strength = tfmp_sensor->reply[4] | (tfmp_sensor->reply[5] <<8);
  tfmp_sensor->temperature = tfmp_sensor->reply[6] | (tfmp_sensor->reply[7] <<8);

  // Convert Temperature to Celsius
  tfmp_sensor->temperature = (tfmp_sensor->temperature >> 3) -256;

  return 0;
}

uint8_t tfmp_calc_checksum(uint8_t *cmd_frame, uint8_t cmd_size){

  //[cmd_size -1] // checksum byte
  //[cmd_size -2] // Limit of checksum calculation

  //Set Checksum to 0
  cmd_frame[cmd_size-1] = 0;

  for(int i=0; i <= cmd_size-2; i++){
    cmd_frame[cmd_size-1] += cmd_frame[i];
  }

  return cmd_frame[cmd_size-1];
}
